import React from 'react';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import FieldSelect from '../../../elements/FieldSelect';
import { optionalCanvassing } from '../../../../utils/dropdownList';
import Dropzone from '../../../elements/Dropzone';
import { Grid } from '@material-ui/core';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  _getInitialValues(values) {
    if (values.additionalCanvasName === 'Lean Canvas') {
      return {
        image: values.leanCanvasUrl,
      };
    }

    return {
      image: values.businessModelUrl,
    };
  }

  render() {
    const {
      errors,
      touched,
      setFieldTouched,
      setFieldValue,
      handleAutosave,
      values,
      onFilesAdded,
      onFilesRemoved,
    } = this.props;

    return (
      <React.Fragment>
        <h3>Canvassing of Product - Additional Canvassing</h3>{' '}
        <p className="sub-text">This page identify the explanation of product canvassing. </p>
        <Field
          name="additionalCanvasName"
          render={({ field }) => (
            <FieldSelect
              {...field}
              dataCy="additional-canvassing"
              error={errors.additionalCanvasName}
              label="Additional Canvassing"
              onBlur={() => setFieldTouched('additionalCanvasName', true)}
              onChange={(e) => {
                setFieldValue('additionalCanvasDesc', '');
                setFieldTouched('additionalCanvasDesc', false, false);
                setFieldValue('additionalCanvasName', e);
              }}
              options={optionalCanvassing}
              placeholder={placeholder.select('canvassing')}
              required
              touched={touched.additionalCanvasName}
            />
          )}
          type="select"
        />
        {values.additionalCanvasName !== '' && (
          <>
            <Grid container style={{ marginBottom: '1rem' }}>
              <Grid item lg={6} md={6} xs={12}>
                <Dropzone
                  dataCy="lean-canvas-image"
                  error={errors.leanCanvasUrl}
                  handleTouch={setFieldTouched}
                  image={values.leanCanvasUrl}
                  initialValues={{
                    bodyText: 'Click or drag image here (format jpg/png)',
                    captionText: '30 MB Maximum',
                  }}
                  isFailed={values.leanCanvas.isFailed}
                  isUploaded={values.leanCanvas.isUploaded}
                  label="Lean Canvas of Product"
                  name="leanCanvasUrl"
                  onFilesAdded={(files) =>
                    onFilesAdded(files, 'leanCanvasUrl', setFieldValue, values)
                  }
                  onFilesRemoved={(files) =>
                    onFilesRemoved(files, 'leanCanvasUrl', setFieldValue, values)
                  }
                  progress={values.leanCanvas.progress}
                  required
                  style={{
                    width: '26rem',
                    display: values.additionalCanvasName !== 'Lean Canvas' ? 'none' : 'flex',
                  }}
                  touched={touched.leanCanvasUrl}
                />
                <Dropzone
                  dataCy="buss-model-image"
                  error={errors.businessModelUrl}
                  handleTouch={setFieldTouched}
                  image={values.businessModelUrl}
                  initialValues={{
                    bodyText: 'Click or drag image here (format jpg/png)',
                    captionText: '30 MB Maximum',
                  }}
                  isFailed={values.businessModel.isFailed}
                  isUploaded={values.businessModel.isUploaded}
                  label="Business Model Canvas of Product"
                  name="businessModelUrl"
                  onFilesAdded={(files) =>
                    onFilesAdded(files, 'businessModelUrl', setFieldValue, values)
                  }
                  onFilesRemoved={(files) =>
                    onFilesRemoved(files, 'businessModelUrl', setFieldValue, values)
                  }
                  progress={values.businessModel.progress}
                  required
                  style={{
                    width: '26rem',
                    display: values.additionalCanvasName === 'Lean Canvas' ? 'none' : 'flex',
                  }}
                  touched={touched.businessModelUrl}
                />
              </Grid>
            </Grid>

            <Field
              name="additionalCanvasDesc"
              render={({ field }) => (
                <FieldText
                  {...field}
                  block
                  data-cy="explanation"
                  error={errors.additionalCanvasDesc}
                  label="Explanation"
                  onChange={(e) => {
                    setFieldTouched('additionalCanvasDesc', true);
                    setFieldValue('additionalCanvasDesc', e.target.value);
                    handleAutosave;
                  }}
                  placeholder={placeholder.longText('explanation', 700)}
                  required
                  touched={touched.additionalCanvasDesc}
                  type="long"
                />
              )}
            />
          </>
        )}
      </React.Fragment>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
