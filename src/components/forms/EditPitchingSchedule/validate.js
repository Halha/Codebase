import { errorHandler } from '../../../constants/copywriting';
import * as Yup from 'yup';

export default Yup.object().shape({
  pitchingDate: Yup.string().required(errorHandler.required('Pitching Schedule')),
  pitchingTimeStart: Yup.string()
    .required(errorHandler.required('Start'))
    .min(5, errorHandler.required('Start')),
  pitchingTimeEnd: Yup.string()
    .required(errorHandler.required('Finish'))
    .min(5, errorHandler.required('Finish')),
  address: Yup.string().required(errorHandler.required('Place')),
});
