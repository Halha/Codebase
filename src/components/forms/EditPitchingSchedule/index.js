import Component from './component';
import validate from './validate';
import { withFormik } from 'formik';

export default withFormik({
  enableReinitialize: true,
  validationSchema: validate,
  handleSubmit: (values, { setSubmitting }) => {
    setTimeout(() => {
      alert(JSON.stringify(values, null, 2));
      setSubmitting(false);
    }, 400);
  },
  displayName: 'pitchingReSchedule',
})(Component);
