import validate from '../validate';

describe('Validate', () => {
  it('return no error given valid values', () => {
    const values = {
      email: 'tes@gmail.com',
      password: '123',
    };
    const assert = {
      email: '',
      password: '',
    };

    expect(validate(values)).toMatchObject(assert);
  });
});
