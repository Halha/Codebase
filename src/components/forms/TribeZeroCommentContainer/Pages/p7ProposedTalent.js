import React from 'react';
import { jobLevel } from '../../../../utils/dropdownList';
import { Field, FieldArray } from 'formik';
import PropTypes from 'prop-types';
import FieldText from '../../../elements/FieldText';
import ProposedSquadField from '../../../fragments/SquadProposedField';
import { placeholder } from '../../../../constants/copywriting';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { values, errors, touched, setFieldValue, setFieldTouched, handleAutosave } = this.props;

    return (
      <>
        <h3>Resource Submission - Proposed Talent</h3>
        <p className="sub-text">
          This page identify the information about talent submission of this business idea.
        </p>
        <FieldArray
          name="squadRequest"
          render={(arrayHelpers) => (
            <ProposedSquadField
              arrayHelpers={arrayHelpers}
              disabled
              errors={errors}
              field={values.squadRequest}
              jobLevelOptions={jobLevel}
              jobRoleOptions={values.jobRoleList}
              setFieldTouched={setFieldTouched}
              setFieldValue={setFieldValue}
              touched={touched}
              values={values}
            />
          )}
        />

        <Field
          name="feedbackSquad"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-talent"
              error={errors.feedbackSquad}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of Proposed Talent"
              onChange={(e) => {
                setFieldTouched('feedbackSquad', true);
                setFieldValue('feedbackSquad', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of proposed talent`, 700)}
              required
              touched={touched.feedbackSquad}
              type="long"
            />
          )}
        />
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
