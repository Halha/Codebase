import React from 'react';
import Grid from '@material-ui/core/Grid';
import {
  areaList,
  typeList,
  customerList,
  businessFieldList,
  productTypeList,
} from '../../../../utils/dropdownList';
import { placeholder } from '../../../../constants/copywriting';
import { Field } from 'formik';
import PropTypes from 'prop-types';
import FieldSelect from '../../../elements/FieldSelect';
import FieldText from '../../../elements/FieldText';
import Dropzone from '../../../elements/Dropzone';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {
      values,
      errors,
      touched,
      setFieldValue,
      setFieldTouched,
      handleAutosave,
      onFilesAdded,
      onFilesRemoved,
    } = this.props;

    return (
      <>
        <h3>General and Background</h3>
        <p className="sub-text">
          This page identify general information about business idea proposals to be submitted.
        </p>
        {/* Business Name Field */}
        <FieldText
          data-cy="idea-name"
          disabled
          label="Name of Business Idea"
          long
          placeholder={placeholder.shortText('name of business idea')}
          required
          value={values.name}
        />

        <Grid className="field-group" container>
          <FieldSelect
            dataCy="product-type"
            disabled
            info="The product's options are Existing Product, New Product, or New product without name."
            label="Product"
            long
            options={productTypeList}
            placeholder={placeholder.select('product')}
            required
            value={values.productType}
          />
          {values.productType === 1 || values.productType === 2 ? (
            <FieldText
              data-cy="product-name"
              disabled
              long
              placeholder={
                values.productType === 1
                  ? placeholder.shortText('name of existing product')
                  : placeholder.shortText('name of new product')
              }
              required
              value={values.productName}
            />
          ) : null}
        </Grid>
        <Grid container style={{ marginBottom: '1rem' }}>
          <Grid item lg={6} md={6} xs={12}>
            <Dropzone
              acceptFormat="image/jpeg,image/png,image/svg+xml"
              dataCy="product-logo"
              disabled
              error={errors.productLogoUrl}
              handleTouch={setFieldTouched}
              image={values.productLogoUrl}
              initialValues={{
                bodyText: 'Click or drag image here (format jpg/png/svg)',
                captionText: '30 MB Maximum',
              }}
              isFailed={values.productLogo.isFailed}
              isUploaded={values.productLogo.isUploaded}
              label="Logo of Product"
              name="productLogoUrl"
              onFilesAdded={(files) => onFilesAdded(files, 'productLogoUrl', setFieldValue, values)}
              onFilesRemoved={(files) =>
                onFilesRemoved(files, 'productLogoUrl', setFieldValue, values)
              }
              progress={values.productLogo.progress}
              style={{ width: '26rem' }}
              touched={touched.productLogoUrl}
            />
          </Grid>
        </Grid>

        <FieldSelect
          dataCy="business-type"
          disabled
          info="The type of business idea's options are Digital Platform or Digital Service."
          label="Type of Business Idea"
          long
          options={typeList}
          placeholder={placeholder.select('type of business idea')}
          required
          value={values.type}
        />

        <FieldSelect
          dataCy="business-area"
          disabled
          info="The area of business idea's options are Business Model, Business Operation, Customer Experience, Platform, or Product/ Service."
          label="Area of Business Idea"
          long
          options={areaList}
          placeholder={placeholder.select('area of business idea')}
          required
          value={values.area}
        />

        <FieldSelect
          creatable
          dataCy="business-field"
          disabled
          info="The field of business idea's options are Advertising, Agriculture, AR/ VR, Big Data/ AI, Blockchain, Communication, or mention another."
          label="Field of Business Idea"
          long
          multiple
          options={businessFieldList}
          placeholder={placeholder.select('field of business idea')}
          required
          value={values.businessField}
        />

        <Grid className="field-group" container>
          <FieldSelect
            dataCy="business-customer"
            disabled
            info="The customer of business idea's options are Enterprise, Government, Telkom Group, Retail/ Consumer, Small &amp; Medium Business, Wholesale, or mention another."
            label="Customer of Business Idea"
            long
            options={customerList}
            placeholder={placeholder.select('customer of business idea')}
            required
            value={values.customer}
          />
          {values.customer === 'Other' ? (
            <FieldText
              data-cy="business-customer-other"
              disabled
              long
              placeholder={placeholder.shortText('other customer of business idea')}
              required
              value={values.otherCustomer}
            />
          ) : null}
        </Grid>

        <Field
          name="feedbackGeneral"
          render={({ field }) => (
            <FieldText
              {...field}
              block
              data-cy="feedback-general"
              error={errors.feedbackGeneral}
              info="This section identifies an evaluation of Tribe Zero regarding the submission of business idea proposals."
              label="Evaluation of General and Background"
              onChange={(e) => {
                setFieldTouched('feedbackGeneral', true);
                setFieldValue('feedbackGeneral', e.target.value);
                handleAutosave;
              }}
              placeholder={placeholder.longText(`evaluation of general and background`, 700)}
              required
              touched={touched.feedbackGeneral}
              type="long"
            />
          )}
        />
      </>
    );
  }
}

Component.propTypes = {
  errors: PropTypes.object.isRequired,
  handleAutosave: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  onFilesAdded: PropTypes.func.isRequired,
  onFilesRemoved: PropTypes.func.isRequired,
  setFieldTouched: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired,
  touched: PropTypes.object.isRequired,
  values: PropTypes.object.isRequired,
};
