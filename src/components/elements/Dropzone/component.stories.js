import React from 'react';
import Dropzone from './index';
import { storiesOf } from '@storybook/react';
import { State, Store } from '@sambego/storybook-state';

const storeNew = new Store({
  image: '',
  progress: 0,
  isUploaded: false,
  isFailed: false
});

const actionsNew = {
  onFilesAdded: (files) => {
    storeNew.set({
      image: files,
      isUploaded: true
    });
  },
  onFilesRemoved: () => {
    storeNew.set({ image: null });
  },
};

storiesOf('Dropzone', module)
  .add('Default', () => (
    <State store={storeNew}>
      <Dropzone
        {...storeNew}
        {...actionsNew}
        initialValues={{
          bodyText: 'Body Text',
          captionText: 'Caption Text'
        }}
      />
    </State>
  ));
