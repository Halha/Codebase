import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import CloseIcon from '@material-ui/icons/Close';
import SnackbarContent from '@material-ui/core/SnackbarContent';
import { ICONS } from '../../../configs';
import { withStyles } from '@material-ui/core/styles';

const styles1 = () => ({
  snackbar: {
    backgroundColor: 'white',
    color: 'black',
    minWidth: 0,
    minHeight: 0,
    maxHeight: '2.5rem',
    padding: '0 0.625rem'
  },

  success: {
    borderLeft: '0.125rem solid #74b816'
  },
  error: {
    borderLeft: '0.125rem solid #f55151'
  },
  warning: {
    borderLeft: '0.125rem solid #fcc419'
  },
  info: {
    borderLeft: '0.125rem solid #229bd8'
  },
  icon: {
    fontSize: '1rem',
    marginLeft: '-1rem',
    marginRight: '0.5rem',
    '&:hover': {
      cursor: 'pointer'
    }
  },
  message: {
    display: 'flex',
    alignItems: 'center',
    color: 'black',
    padding: 0,
    marginRight: '0.5rem'
  }
});

const StyledSnackbarContentWrapper = withStyles({
  root: {
    boxShadow: '0 0.1875rem 0.375rem 0 rgba(72, 122, 157, 0.3)',
    borderRadius: '0.3125rem'
  }
})(SnackbarContent);

function MySnackbarContent(props) {
  const { classes, className, message, onClose, variant, ...other } = props;

  return (
    <StyledSnackbarContentWrapper
      action={[
        <CloseIcon
          aria-label="Close"
          className={classes.icon}
          color="inherit"
          key="close"
          onClick={onClose}
        />
      ]}
      aria-describedby="client-snackbar"
      className={`${classNames(classes[variant])} ${classNames(classes['snackbar'])} ${className}`}
      message={
        <span className={classes.message} id="client-snackbar">
          <div
            style={{
              height: '1.5rem',
              width: '1.5rem',
              alignItems: 'center',
              justifyContent: 'center',
              display: 'flex',
              marginRight: '0.5rem'
            }}
          >
            {variant === 'success' || variant === 'error' ? (
              <img src={variant === 'success' ? ICONS.CHECK_FILL_ICON : ICONS.SNACKBAR_WARNING} />
            ) : null}
          </div>
          {message}
        </span>
      }
      {...other}
    />
  );
}

MySnackbarContent.defaultProps = {
  className: {},
  message: ''
};

MySnackbarContent.propTypes = {
  classes: PropTypes.object.isRequired,
  className: PropTypes.string,
  message: PropTypes.node,
  onClose: PropTypes.func.isRequired,
  variant: PropTypes.oneOf(['success', 'warning', 'error', 'info']).isRequired
};

export const SnackbarContentWrapper = withStyles(styles1)(MySnackbarContent);
