import React from 'react';
import PropTypes from 'prop-types';
import styles from './styles.module.scss';

export default class Component extends React.Component {
  render() {
    const { children, className } = this.props;

    return <div className={`${className} ${styles['error-message']}`}>{children}</div>;
  }
}

Component.defaultProps = {
  children: null,
  className: '',
};

Component.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
