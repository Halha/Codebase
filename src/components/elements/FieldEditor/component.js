import React from 'react';
import PropTypes from 'prop-types';
import { convertToHTML, convertFromHTML } from 'draft-convert';
import Grid from '@material-ui/core/Grid';
import ErrorMessage from '../ErrorMessage';
import './styles.scss';
import { Editor, EditorState, RichUtils } from 'draft-js';
import { ICONS } from '../../../configs';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this._getClass = this._getClass.bind(this);
    this._handleStyleClick = this._handleStyleClick.bind(this);
    this.state = {
      editorState: EditorState.createWithContent(convertFromHTML(this.props.value)),
      rawValue: this.props.value.replace(/<[^>]+>/g, '') || '',
    };
    this._handleChange = (editorState) => {
      const value = convertToHTML(editorState.getCurrentContent());
      let val = value;
      const rawValue = val.replace(/<[^>]+>/g, '');
      this.setState({ editorState, rawValue });
      this.props.onChange(value, rawValue);
    };
    this.handleKeyCommand = this.handleKeyCommand.bind(this);
  }

  _handleStyleClick(state) {
    this._handleChange(RichUtils.toggleInlineStyle(this.state.editorState, state));
  }

  handleKeyCommand(command, editorState) {
    const newState = RichUtils.handleKeyCommand(editorState, command);
    if (newState) {
      this._handleChange(newState);
      return 'handled';
    }
    return 'not-handled';
  }

  _handleBlur = (editorState) => {
    this.props.onBlur(editorState);
  };

  _getClass() {
    let className = 'cdx-input';
    if (this.props.error && this.props.touched) {
      className = `${className} error-input`;
    }
    if (this.props.disabled) {
      className = `${className} disabled`;
    }

    return className;
  }

  _getMainClass() {
    let className = 'cdx-texteditor';
    if (this.props.block) {
      className = `${className} block`;
    }

    return className;
  }

  _checkActiveStyling(style) {
    return this.state.editorState.getCurrentInlineStyle().has(style);
  }

  _renderCounter(char) {
    return (
      <p
        className="counter"
        style={this.props.error && this.props.touched ? { color: '#f55151' } : {}}
      >
        {this.props.limit - char}
      </p>
    );
  }

  render() {
    const { className, field, label, touched, error, style, placeholder, disabled } = this.props;

    return (
      <div className={`${this._getMainClass()} ${className}`} direction="column" style={style}>
        <Grid align="end" className="label" container justify="space-between">
          {label !== '' ? <p>{label}</p> : <p style={{ color: '#00000000' }}>.</p>}
          {this._renderCounter(this.state.rawValue.length)}
        </Grid>
        <div className={this._getClass()}>
          <div className="menu-bar">
            <button
              className={this._checkActiveStyling('BOLD') ? 'is-active' : ''}
              disabled={disabled}
              onClick={() => this._handleStyleClick('BOLD')}
              type="button"
            >
              <img src={ICONS.ICON_BOLD} />
            </button>
            <button
              className={this._checkActiveStyling('ITALIC') ? 'is-active' : ''}
              disabled={disabled}
              onClick={() => this._handleStyleClick('ITALIC')}
              type="button"
            >
              <img src={ICONS.ICON_ITALIC} />
            </button>
            <button
              className={this._checkActiveStyling('UNDERLINE') ? 'is-active' : ''}
              disabled={disabled}
              onClick={() => this._handleStyleClick('UNDERLINE')}
              type="button"
            >
              <img src={ICONS.ICON_UNDERLINE} />
            </button>
          </div>
          <Editor
            {...field}
            editorState={this.state.editorState}
            handleKeyCommand={this.handleKeyCommand}
            onBlur={this._handleBlur}
            onChange={this._handleChange}
            placeholder={placeholder}
            readOnly={disabled}
          />
        </div>
        {touched && error && <ErrorMessage>{error}</ErrorMessage>}
      </div>
    );
  }
}

Component.defaultProps = {
  block: false,
  className: '',
  disabled: false,
  error: '',
  field: {},
  label: '',
  limit: 700,
  placeholder: '',
  style: {},
  touched: false,
  value: '',
};

Component.propTypes = {
  block: PropTypes.bool,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  error: PropTypes.string,
  field: PropTypes.object,
  label: PropTypes.string,
  limit: PropTypes.number,
  onBlur: PropTypes.func.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  style: PropTypes.object,
  touched: PropTypes.bool,
  value: PropTypes.string,
};
