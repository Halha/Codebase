import React from 'react';
import PropTypes from 'prop-types';
import Accordion from '../../elements/Accordion';
import ProgressBar from '../../elements/ProgressBar';
import styles from './styles.css';

export default class Component extends React.Component {
  _renderOkr(data) {
    return (
      <table className={styles.table}>
        <thead>
          <tr className={styles['head-row']}>
            <th className={styles['objective-column']}>Key Result</th>
            <th>Progress</th>
          </tr>
        </thead>
        <tbody className={styles.tbody}>
          {data.map((item, idx) => (
            <tr key={idx}>
              <td className={styles['objective-column']}>({idx+1}). {item.keyResultName}</td>
              <td>
                <ProgressBar percentage={item.keyResultProgress} />
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    );
  }
  
  render() {
    const { data, isLoading } = this.props;
    

    if (isLoading || !data.length)
      return null;

    return (
      data.map((item, idx) => (
        <Accordion
          desc={this._renderOkr(item.keyResult)}
          key={idx}
          name={
            <>
              <h4 className={styles['header-title']}><strong>Objective : </strong>{item.objectiveName}</h4>
              <ProgressBar percentage={item.objectiveProgress} />
            </>
          } />
      ))
    );
  }
}

Component.defaultProps = {
  data: [],
  isLoading: true,
};

Component.propTypes = {
  data: PropTypes.array,
  isLoading: PropTypes.bool,
};
