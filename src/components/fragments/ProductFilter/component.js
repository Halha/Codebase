import React from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import Button from '../../elements/Button';
import Select from '../../elements/Select';
import styles from './styles.css';

export default class Component extends React.Component {
  _renderOptionProject() {
    const { data, handleChangeOption } = this.props;
    const query = queryString.parse(location.search);
    let newProduct = [{ id: undefined, name: 'All' }];

    if (data.product) {
      const value = query.productId ? parseInt(query.productId) : newProduct[0].id;
      data.product.map(item => {
        newProduct.push({ id: item.productId, name: item.productName });
      });

      return (
        <div>
          <h4 className={styles['title-description']}>Product Name</h4>
          <Select
            className={styles['select-project-filter']}
            data={newProduct}
            name="productId"
            onChange={handleChangeOption}
            value={value}
            width="15.75rem" />
        </div>
      );
    } else return null;
  }

  _renderOptionTribe() {
    const { data, handleChangeOption } = this.props;
    const query = queryString.parse(location.search);
    let newProduct = [{ id: undefined, name: 'All' }];

    if (data.tribe) {
      const value = query.tribeId ? parseInt(query.tribeId) : newProduct[0].id;
      data.tribe.map(item => {
        newProduct.push({ id: item.tribeId, name: item.tribeName });
      });

      return (
        <div>
          <h4 className={styles['title-description']}>Tribe Name</h4>
          <Select
            className={styles['select-project-filter']}
            data={newProduct}
            name="tribeId"
            onChange={handleChangeOption}
            value={value}
            width="15.75rem" />
        </div>
      );
    } else return null;
  }

  _handleReset = () => {
    this.props.handleChangeOption({ name: 'productId', value: undefined });
    this.props.handleChangeOption({ name: 'tribeId', value: undefined });
  }

  render() {
    const query = queryString.parse(location.search);

    return (
      <div className={styles.container}>
        {this._renderOptionProject()}
        {this._renderOptionTribe()}
        { query.productId || query.tribeId ?
          <Button
            className={styles['reset-button']}
            onClick={this._handleReset}
          >Reset</Button> : ''}
      </div>
    );
  }
}

Component.propTypes = {
  classes: PropTypes.object,
  data: PropTypes.object,
  handleChangeOption: PropTypes.func,
  isLoading: PropTypes.object,
};

Component.defaultProps = {
  classes: {},
  data: {
    product: null,
    tribe: null,
  },
  handleChangeOption: '',
  isLoading: {
    product: true,
    tribe: true,
  },
};
