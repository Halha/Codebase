import React from 'react';
import PropTypes from 'prop-types';

export default function ArrowLast(props) {
  const disabled = !props.disabled ? '#1E2025' : '#C8D0E0';

  return (
    <svg height="1.5rem" viewBox="0 0 24 24" width="1.5rem" xmlns="http://www.w3.org/2000/svg" >
      <g fill="none" fillRule="evenodd">
        <path d="M14.317 11.924l-5.62-5.217a.67.67 0 0 0-.912.982l4.562 4.235a.74.74 0 0 1 0 1.085l-4.605 4.277a.67.67 0 0 0 .912.982l5.663-5.259a.74.74 0 0 0 0-1.085z" fill={disabled} fillRule="nonzero" />
        <path d="M17.5 17.744V6.528" stroke={disabled} strokeLinecap="round" />
      </g>
    </svg>
  );
}

ArrowLast.defaultProps = {
  disabled: false
};

ArrowLast.propTypes = {
  disabled: PropTypes.bool,
};
